# Project android-automation-2-2

This project contains automation tests for Stockbit Android Automation. It is built using intelliJIDEA and requires certain dependencies to be installed.

## Dependencies

- [Appium](http://appium.io/): Appium is an open-source tool for automating mobile applications. It is used for mobile test automation in this project.

- [Cucumber](https://cucumber.io) : Cucumber is a testing framework that supports Behavior-Driven Development (BDD), allowing you to write test cases in a natural language format

- [JUnit](https://junit.org/junit4/) : this project using JUnit framework for Java that is widely used for unit testing. we can use its classes and annotations to write and execute tests.

- [Selenium](https://www.selenium.dev) :  this project using classes and methods for automation in your Java project.
## Setup Instructions

Follow the steps below to set up your environment and run the automation tests.

### 1. Install Appium

#### Using npm:

```bash
npm install -g appium

```

#### Appium Inspector

set Desired Capability in Appium Inspector
``` 
{
"platformName": "Android",
"appium:platformVersion": "12",
"appium:automationName": "UiAutomator2",
"appium:deviceName": "Android Emulator"
} 
```

### 2. Cucumber
This project using gradle, here the step :
Open the build.gradle file of your Gradle project and add the following dependencies for Cucumber:

```
dependencies {
    implementation 'io.cucumber:cucumber-java:7.3.0' // Replace with the latest version
    implementation 'io.cucumber:cucumber-junit:7.3.0' // Replace with the latest version
    testImplementation 'junit:junit:4.13.1' // Include JUnit for running Cucumber tests
}
```
### 3. JUnit
This project using gradle, here the step :
Open the build.gradle file of your Gradle project and add the following dependencies for JUnit:

```
dependencies {
    testImplementation 'junit:junit:4.13.1' // Replace with the latest version
}
```
### 4. Selenium
This project using gradle, here the step :
Open the build.gradle file of your Gradle project and add the following dependencies for JUnit:

```
dependencies {
    testImplementation 'org.seleniumhq.selenium:selenium-java:3.141.59' // Replace with the latest version
}
```

## How To Run Automation

##### 1. Run Appium
```
→ appium
```
##### 2. Open Simulator in Android Studio
```
1. makesure the virtual device is installed tap button play
2. makesure its already create session to appium
```

#### 3. Open the project
```
→ Login.feature
→ Click Run 'Scenario:Success login'
```

