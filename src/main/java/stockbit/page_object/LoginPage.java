package stockbit.page_object;

import org.openqa.selenium.By;

public class LoginPage extends BasePage {
    public void isOnboardingPage() { assertIsDisplay(By.id("com.stockbit.android:id/ivWellcomeImageHeader"));
    }
    public void tapLogin() {
        tap(By.id("com.stockbit.android:id/btnWellcomeLogIn"));
    }

    public void inputUsername(String username) {
        typeOn(By.xpath("(//android.widget.EditText[@resource-id='com.stockbit.android:id/tiet_text_field_input'])[1]"), username);
    }

    public void inputPassword(String password) {
        typeOn(By.xpath("(//android.widget.EditText[@resource-id='com.stockbit.android:id/tiet_text_field_input'])[2]"), password);
    }

    public void tapLoginButton() {
        tap(By.id("com.stockbit.android:id/cl_layout_parent"));
    }

    public void tapSkipBiometricPopup() {
        tap(By.id("com.stockbit.android:id/btn_smart_login_skip"));
    }

    public void tapSkipAvatarPopup() {
        tap(By.id("com.stockbit.android:id/btn_skip_choose_avatar"));
    }

    public void isWatchlistPage() {
        tapSkipBiometricPopup();
        tapSkipAvatarPopup();
        assertIsDisplay(By.xpath("//android.widget.TextView[@text='All Watchlist']"));
    }
}

